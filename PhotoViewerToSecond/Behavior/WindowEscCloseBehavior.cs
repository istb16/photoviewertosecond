﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace PhotoViewerToSecond.Behavior
{
    class WindowEscCloseBehavior : Behavior<Window>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.KeyDown += AssociatedObject_KeyDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.KeyDown -= AssociatedObject_KeyDown;
        }

        void AssociatedObject_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) AssociatedObject.Close();
        }
    }
}
