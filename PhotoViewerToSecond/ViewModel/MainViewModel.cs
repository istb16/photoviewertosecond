﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace PhotoViewerToSecond.ViewModel
{
    class MainViewModel : BaseViewModel
    {
        public class ImageFile : BaseViewModel
        {
            public string FileName { get; set; }
            private string filePath;
            public string FilePath
            {
                get { return filePath; }
                set
                {
                    filePath = value;
                    OnPropertyChanged("FilePath");

                    // 画像サムネイル
                    if (Util.FileUtil.IsImage(filePath))
                    {
                        if (Thumb == null) Thumb = new BitmapImage();
                        Thumb.BeginInit();
                        Thumb.CacheOption = BitmapCacheOption.OnLoad;
                        Thumb.CreateOptions = BitmapCreateOptions.None;
                        Thumb.UriSource = new Uri(filePath);
                        Thumb.DecodePixelWidth = 250;
                        Thumb.EndInit();
                        Thumb.Freeze();
                    }
                    // 動画サムネイル
                    else
                    {
                        // プレイヤーを開き、特定の位置で停止させる
                        var player = new MediaPlayer { ScrubbingEnabled = true };
                        player.Volume = 0;
                        player.Open(new Uri(filePath, UriKind.Absolute));
                        player.Pause();
                        // サムネイル位置のセット
                        while (!player.NaturalDuration.HasTimeSpan) System.Threading.Thread.Sleep(100);
                        player.Position = TimeSpan.FromSeconds(player.NaturalDuration.TimeSpan.TotalSeconds / 2);
                        // リサイズ後のサイズを計算
                        while (player.DownloadProgress < 1.0 || player.NaturalVideoWidth == 0) System.Threading.Thread.Sleep(100);
                        var ratio = (double)250 / player.NaturalVideoWidth;
                        int width = (int)(player.NaturalVideoWidth * ratio);
                        int height = (int)(player.NaturalVideoHeight * ratio);
                        // ビットマップにvisualを経由して描画する
                        var visual = new DrawingVisual();
                        using (var context = visual.RenderOpen()) context.DrawVideo(player, new System.Windows.Rect(0, 0, width, height));
                        var renderTargetBitmap = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Pbgra32);
                        System.Threading.Thread.Sleep(100); // おまじない
                        renderTargetBitmap.Render(visual);
                        // PNGに変換し、Thumbへ書き込む
                        var encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                        using (var stream = new MemoryStream())
                        {
                            encoder.Save(stream);
                            stream.Seek(0, SeekOrigin.Begin);

                            if (Thumb == null) Thumb = new BitmapImage();
                            Thumb.BeginInit();
                            Thumb.CacheOption = BitmapCacheOption.OnLoad;
                            Thumb.CreateOptions = BitmapCreateOptions.None;
                            Thumb.StreamSource = stream;
                            Thumb.EndInit();
                            Thumb.Freeze();
                        }
                    }
                }
            }
            public BitmapImage Thumb { get; set; }
        }
        // 画像ファイル
        public ObservableCollection<ImageFile> ImageFiles { get; set; }
        // 選択された画像ファイル
        public ImageFile SelectedImageFile { get; set; }

        public class Screen : BaseViewModel
        {
            public string ScreenName { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public bool IsPrimary { get; set; }
        }
        // ディスプレイリスト
        public ObservableCollection<Screen> Screens { get; set; }
        // 選択されたディスプレイ
        private Screen selectedScreen;
        public Screen SelectedScreen
        {
            get { return selectedScreen; }
            set
            {
                selectedScreen = value;
                Util.AppSetting.Set(Util.AppSettingKeys.ScreenName, (value == null) ? null : value.ScreenName);
                OnPropertyChanged("SelectedScreen");
            }
        }
        // 選択されたサムネイルサイズ
        private int selectedThumbSize;
        public int SelectedThumbSize
        {
            get { return selectedThumbSize; }
            set
            {
                selectedThumbSize = value;
                Util.AppSetting.SetInt(Util.AppSettingKeys.ThumbSize, value);
                OnPropertyChanged("SelectedThumbSize");
            }
        }

        // 選択されたフルスクリーン表示フラグ
        private bool selectedIsFullScreen;
        public bool SelectedIsFullScreen
        {
            get { return selectedIsFullScreen; }
            set
            {
                selectedIsFullScreen = value;
                Util.AppSetting.SetBool(Util.AppSettingKeys.IsFullScreen, value);
                OnPropertyChanged("SelectedIsFullScreen");
            }
        }

        // ビデオの現在再生時間
        private TimeSpan videoPosition;
        public TimeSpan VideoPosition
        {
            get { return videoPosition; }
            set
            {
                videoPosition = value;
                OnPropertyChanged("VideoPosition");
            }
        }

        // ビデオのトータル再生時間
        private TimeSpan videoTotalTime;
        public TimeSpan VideoTotalTime
        {
            get { return videoTotalTime; }
            set
            {
                videoTotalTime = value;
                OnPropertyChanged("VideoTotalTime");
            }
        }

        public MainViewModel()
        {
            double dpi =
                PresentationSource.FromVisual(Application.Current.MainWindow).CompositionTarget.TransformFromDevice.M11;

            // 初期化
            ImageFiles = new ObservableCollection<ImageFile>();
            SelectedImageFile = null;
            Screens = new ObservableCollection<Screen>();
            foreach (var screen in System.Windows.Forms.Screen.AllScreens)
            {
                var screenName = screen.DeviceName;
                if (string.IsNullOrWhiteSpace(screenName)) screenName = "NAMELESS";
                else if (screenName.LastIndexOf("\\") >= 0) screenName = screenName.Remove(0, screenName.LastIndexOf("\\") + 1);

                Screens.Add(new Screen()
                {
                    ScreenName = screenName,
                    X = (int)((double)screen.WorkingArea.X * dpi),
                    Y = (int)((double)screen.WorkingArea.Y * dpi),
                    Width = screen.WorkingArea.Width,
                    Height = screen.WorkingArea.Height,
                    IsPrimary = screen.Primary,
                });
            }
            if (Screens.Count <= 0) throw new ApplicationException("Can not find screen.");
            else
            {
                var selectedName = Util.AppSetting.Get(Util.AppSettingKeys.ScreenName);
                var selected = Screens.FirstOrDefault(x => x.ScreenName == selectedName);
                if (selected != null) SelectedScreen = selected;
            }
            SelectedThumbSize = Util.AppSetting.GetInt(Util.AppSettingKeys.ThumbSize);
            if (SelectedThumbSize < 30) SelectedThumbSize = 30;
            SelectedIsFullScreen = Util.AppSetting.GetBool(Util.AppSettingKeys.IsFullScreen);
        }

        public bool AddImageFile(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) return false;

            // 拡張子判定
            var isImage = Util.FileUtil.IsImage(path);
            var isVideo = Util.FileUtil.IsVideo(path);
            if (!isImage && !isVideo) return false;

            // ファイル登録
            var data = new ImageFile()
            {
                FileName = Path.GetFileName(path),
                FilePath = path
            };
            ImageFiles.Add(data);
            SelectedImageFile = data;
            return true;
        }

        public bool DeleteSelectedImageFile()
        {
            var idx = ImageFiles.IndexOf(SelectedImageFile);
            if (idx < 0) return false;

            ImageFiles.Remove(SelectedImageFile);
            var len = ImageFiles.Count();
            if (len <= 0) SelectedImageFile = null;
            else SelectedImageFile = ImageFiles[(len <= idx) ? len - 1 : idx];

            return true;
        }
    }
}
