﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace PhotoViewerToSecond.ViewModel
{
    class ImageViewModel : BaseViewModel
    {
        private string imagePath;
        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                imagePath = value;
                OnPropertyChanged("ImagePath");
                OnPropertyChanged("VideoVisibility");
                OnPropertyChanged("ImageVisibility");
            }
        }

        public System.Windows.Visibility VideoVisibility
        {
            get
            {
                return Util.FileUtil.IsVideo(imagePath) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            }
        }

        public System.Windows.Visibility ImageVisibility
        {
            get
            {
                return Util.FileUtil.IsImage(imagePath) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            }
        }
    }
}
