﻿using System.ComponentModel;

namespace PhotoViewerToSecond.ViewModel
{
    class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (string name in propertyNames) PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
