﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoViewerToSecond
{
    /// <summary>
    /// ImageWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ImageWindow : Window
    {
        private ViewModel.ImageViewModel vm = new ViewModel.ImageViewModel();
        private bool isPlayed = false;

        public ImageWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        public ImageWindow(string filePath) : this()
        {
            SetImage(filePath);
        }

        public bool SetImage(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath)) return false;
            vm.ImagePath = filePath;
            return true;
        }

        public void SetFullScreen(bool isFull)
        {
            if (isFull)
            {
                WindowStyle = WindowStyle.None;
                WindowState = WindowState.Maximized;
                ShowInTaskbar = false;
            }
            else
            {
                WindowStyle = WindowStyle.SingleBorderWindow;
                WindowState = WindowState.Normal;
                ShowInTaskbar = true;
            }
        }

        public void VideoPlay()
        {
            video.Play();
            isPlayed = true;
        }

        public void VideoPause()
        {
            video.Pause();
            isPlayed = false;
        }

        public void VideoStop()
        {
            video.Stop();
            isPlayed = false;
        }

        public bool IsPlayed()
        {
            return isPlayed;
        }

        public bool IsVideo()
        {
            return vm.VideoVisibility == Visibility.Visible;
        }

        public void SetVideoPosition(TimeSpan span)
        {
            video.Position = span;
        }

        public TimeSpan GetVideoPosition()
        {
            return video.Position;
        }

        public TimeSpan GetVideoTotal()
        {
            return (video.NaturalDuration.HasTimeSpan) ? video.NaturalDuration.TimeSpan : TimeSpan.MinValue;
        }

        #region notify closed event
        public delegate void OnWindowClosedEventHandler(object sender, EventArgs e);
        public event OnWindowClosedEventHandler OnWindowClosed;

        private void Window_Closed(object sender, EventArgs e)
        {
            VideoStop();
            OnWindowClosed(this, e);
        }
        #endregion

        #region notify media ended event
        public delegate void OnMediaEndedEventHandler(object sender, RoutedEventArgs e);
        public event OnMediaEndedEventHandler OnMediaEnded;

        private void video_MediaEnded(object sender, RoutedEventArgs e)
        {
            OnMediaEnded(this, e);
        }
        #endregion
    }
}
