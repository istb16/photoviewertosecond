﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PhotoViewerToSecond
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel.MainViewModel vm = null;
        ImageWindow iw = null;
        private DispatcherTimer timer = null;

        #region ウィンドウ
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vm = new ViewModel.MainViewModel();

            DataContext = vm;
            timer = new DispatcherTimer(DispatcherPriority.Background);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 400);
            timer.Tick += Idle_VideoPosition;
            timer.Start();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CloseImageView();
            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }
        }
        #endregion

        #region ファイルのドラッグ＆ドロップ
        private void Window_Drop(object sender, DragEventArgs e)
        {
            var dropFiles = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (dropFiles != null)
            {
                foreach (var df in dropFiles) vm.AddImageFile(df);
            }
        }

        private void Window_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true)) e.Effects = DragDropEffects.Copy;
            else e.Effects = DragDropEffects.None;
            e.Handled = true;
        }
        #endregion

        #region メディア表示
        private void DisplayImage_Click(object sender, RoutedEventArgs e)
        {
            OpenImageView();
        }

        private void HideImage_Click(object sender, RoutedEventArgs e)
        {
            CloseImageView();
        }

        private void OpenImageView()
        {
            if (vm.SelectedImageFile != null)
            {
                // メディア表示処理
                if (iw == null)
                {
                    iw = new ImageWindow(vm.SelectedImageFile.FilePath);
                    iw.OnWindowClosed += delegate (object sender, EventArgs e) {
                        // メディア表示終了処理
                        iw = null;
                        VideoControll.Visibility = Visibility.Collapsed;
                    };
                    iw.OnMediaEnded += delegate (object sender, RoutedEventArgs e)
                    {
                        // ビデオ停止イベント
                        StopVideo_Click(sender, e);
                    };
                    if (vm.SelectedScreen != null)
                    {
                        var sc = vm.SelectedScreen;
                        iw.Left = sc.X;
                        iw.Top = sc.Y;
                        iw.SourceInitialized += (sender, arg) =>
                        {
                            iw.SetFullScreen(vm.SelectedIsFullScreen);
                        };
                    }
                    iw.Show();
                }
                else
                {
                    iw.SetImage(vm.SelectedImageFile.FilePath);
                }
                
                // 画像／ビデオ別処理
                if (Util.FileUtil.IsVideo(vm.SelectedImageFile.FilePath))
                {
                    VideoControll.Visibility = Visibility.Visible;
                    StartVideo.Visibility = Visibility.Visible;
                    PauseVideo.Visibility = Visibility.Collapsed;
                }
                else
                {
                    VideoControll.Visibility = Visibility.Collapsed;
                }

                iw.Activate();
                iw.Topmost = true;
                iw.Topmost = false;
                iw.Focus();
            }
        }

        private void CloseImageView()
        {
            if (iw != null)
            {
                iw.Close();
            }
        }
        #endregion

        private void ListBox_KeyUp(object sender, KeyEventArgs e)
        {
            // 選択画像の削除
            if (e.Key == Key.Delete) vm.DeleteSelectedImageFile();
        }

        #region ビデオ操作
        private void StartVideo_Click(object sender, RoutedEventArgs e)
        {
            iw.VideoPlay();
            StartVideo.Visibility = Visibility.Collapsed;
            PauseVideo.Visibility = Visibility.Visible;
        }

        private void StopVideo_Click(object sender, RoutedEventArgs e)
        {
            iw.VideoStop();
            StartVideo.Visibility = Visibility.Visible;
            PauseVideo.Visibility = Visibility.Collapsed;
        }

        private void PauseVideo_Click(object sender, RoutedEventArgs e)
        {
            iw.VideoPause();
            StartVideo.Visibility = Visibility.Visible;
            PauseVideo.Visibility = Visibility.Collapsed;
        }

        private void RewindVideo_Click(object sender, RoutedEventArgs e)
        {
            iw.SetVideoPosition(iw.GetVideoPosition().Add(new TimeSpan(0, 0, -10)));
        }

        private void ForwardVideo_Click(object sender, RoutedEventArgs e)
        {
            iw.SetVideoPosition(iw.GetVideoPosition().Add(new TimeSpan(0, 0, 10)));
        }

        private void Idle_VideoPosition(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke( () =>
            {
                if ((iw != null) && iw.IsVideo())
                {
                    vm.VideoPosition = iw.GetVideoPosition();
                    vm.VideoTotalTime = iw.GetVideoTotal();
                }
                else
                {
                    vm.VideoPosition = TimeSpan.MinValue;
                    vm.VideoTotalTime = TimeSpan.MinValue;
                }
            });
        }
        #endregion
    }
}
