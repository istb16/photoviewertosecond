﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhotoViewerToSecond.Util
{
    /// <summary>
    /// アプリケーション設定キー
    /// </summary>
    public enum AppSettingKeys
    {
        ThumbSize,
        ScreenName,
        IsFullScreen
    }

    /// <summary>
    /// システム設定
    /// </summary>
    public static class AppSetting
    {
        static readonly Dictionary<AppSettingKeys, string> mappingKeyNames = new Dictionary<AppSettingKeys, string>()
        {
            {AppSettingKeys.ThumbSize, "SelectedThumbnailSize"},
            {AppSettingKeys.ScreenName, "SelectedDisplayScreenName" },
            {AppSettingKeys.IsFullScreen, "SelectedIsFullScreen" }
        };

        /// <summary>
        /// 設定値の取得
        /// </summary>
        /// <param name="key">アプリケーション設定キー</param>
        /// <returns>設定値</returns>
        public static string Get(AppSettingKeys key)
        {
            return ConfigurationManager.AppSettings[mappingKeyNames[key]];
        }
        
        /// <summary>
        /// 設定値の取得
        /// </summary>
        /// <param name="key">アプリケーション設定キー</param>
        /// <returns>設定値</returns>
        public static int GetInt(AppSettingKeys key)
        {
            int val = 0;
            int.TryParse(Get(key), out val);
            return val;
        }

        /// <summary>
        /// 設定値の取得
        /// </summary>
        /// <param name="key">アプリケーション設定キー</param>
        /// <returns>設定値</returns>
        public static double GetDouble(AppSettingKeys key)
        {
            double val = 0;
            double.TryParse(Get(key), out val);
            return val;
        }

        /// <summary>
        /// 設定値の取得
        /// </summary>
        /// <param name="key">アプリケーション設定キー</param>
        /// <returns>設定値</returns>
        public static bool GetBool(AppSettingKeys key)
        {
            bool val = false;
            bool.TryParse(Get(key), out val);
            return val;
        }

        /// <summary>
        /// 値の設定
        /// </summary>
        /// <param name="key">アプリケーション設定キー</param>
        /// <param name="value">設定値</param>
        public static void Set(AppSettingKeys key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var keyName = mappingKeyNames[key];
            if (value != null)

            {
                if (config.AppSettings.Settings.AllKeys.Contains(keyName))
                {
                    config.AppSettings.Settings[keyName].Value = value;
                }
                else
                {
                    config.AppSettings.Settings.Add(keyName, value);
                }
            }
            else config.AppSettings.Settings.Remove(mappingKeyNames[key]);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// 値の設定
        /// </summary>
        /// <param name="key">アプリケーション設定キー</param>
        /// <param name="value">設定値</param>
        public static void SetInt(AppSettingKeys key, int value)
        {
            Set(key, value.ToString());
        }

        /// <summary>
        /// 値の設定
        /// </summary>
        /// <param name="key">アプリケーション設定キー</param>
        /// <param name="value">設定値</param>
        public static void SetBool(AppSettingKeys key, bool value)
        {
            Set(key, value.ToString());
        }
    }
}
