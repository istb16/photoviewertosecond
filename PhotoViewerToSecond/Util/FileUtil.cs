﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PhotoViewerToSecond.Util
{
    /// <summary>
    /// システム設定
    /// </summary>
    public static class FileUtil
    {
        public static bool IsImage(string file)
        {
            if (string.IsNullOrWhiteSpace(file)) return false;
            var ext = System.IO.Path.GetExtension(file).ToLower();
            if ((ext == ".jpg") || (ext == ".jpeg") || (ext == ".png") || (ext == ".bmp") || (ext == ".gif")) return true;
            return false;
        }

        public static bool IsVideo(string file)
        {
            if (string.IsNullOrWhiteSpace(file)) return false;
            var ext = System.IO.Path.GetExtension(file).ToLower();
            if ((ext == ".mp4") || (ext == ".wmv") || (ext == ".avi")) return true;
            return false;
        }
    }
}
